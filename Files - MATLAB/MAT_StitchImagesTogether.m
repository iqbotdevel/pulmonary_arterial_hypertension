%%   Yagnik Patel   |    Project 6 - Pulmonary Aterial Hypertension  |     Compile SVS Tiles into Single Image
close all hidden;
clear;
clc;

%% Start_File

%% Directory
     %Parent_Folder_Path = uigetdir;
     Parent_Folder_Path = 'C:\Users\Yagnik Patel\Desktop\ImageIQ\_Projects\6_Pulmonary_Arterial_Hypertension\Images\85930';
     [~,IM_Num,~] = fileparts(Parent_Folder_Path);
          
%% List - Tiles    
     TILES_Search = dir(fullfile(Parent_Folder_Path, '*.tif'));
     TILES_List = {TILES_Search.name}';

%% Find Number of Rows & Columns             
temp = regexp(TILES_List, '[i_j.]', 'split');
temp2 = vertcat(temp{:});
maxCols = max(str2double(temp2(:,3)));
maxRows = max(str2double(temp2(:,5)));

%% Arrange Tiles Into Single Image
imgCell = cell(maxRows,maxCols);
for idxCol = 1:maxCols
     for idxRows = 1:maxRows
          imageName = sprintf('%s%s%s%i%s%i%s',Parent_Folder_Path,IM_Num,'_i',idxCol,'_j',idxRows,'.tif');
          %imgCell{idxRows,idxCol} = imread(imageName);
     end
end

bigImage = mat2cell(imgCell);
imshow(bigImage);