function SVS2TIFF(tilew,tileh)
[fname_orig, path_orig] = uigetfile('*.svs','Choose first Aperio SVS file');
if (fname_orig ~= 0)
    d_orig = dir(strcat(path_orig,'*.svs'));
end
numfiles_orig = size(d_orig,1);
if nargin<1
    tilew=10000;
end
if nargin<2
    tileh=10000;
end
for ff = 1:numfiles_orig
    fprintf(2,'Processing %s: file %s  of %s\n',d_orig(ff).name,num2str(ff),num2str(numfiles_orig));
    inpath =strcat(path_orig,d_orig(ff).name);
    [s, out]=dos(['openslide-show-properties.exe',' ','"',inpath,'"']);
    if s==0
        dout=double(out);
        k=find(dout==10);
        for j=1:length(k)-1
            line=out(k(j)+1:k(j+1)-1);
            if strmatch('openslide.level[0].height', line)
                height = strrep(line,'openslide.level[0].height: ','');
                height=str2double(strrep(height,'''',''));
                display(height)
            end
            if strmatch('openslide.level[0].width', line)
                width = strrep(line,'openslide.level[0].width: ','');
                width=str2double(strrep(width,'''',''));
                display(width)
            end
            if strmatch('openslide.mpp-x', line)
                resx = strrep(line,'openslide.mpp-x: ','');
                resx=str2double(strrep(resx,'''',''));     
            end
            if strmatch('openslide.mpp-y', line)
                resy = strrep(line,'openslide.mpp-y: ','');
                resy=str2double(strrep(resy,'''',''));     
            end
        end
        level='0';
        outpngbase=strrep(inpath,'.svs','');
        
        %write out png file
        num_tilew=ceil(width/tilew);
        num_tileh=ceil(height/tileh);
        
        for j=1:num_tileh
            for i=1:num_tilew
                %openslide-write-png.exe [OPTIONS...] slide x y level width
                %height output.png
                posx=(i-1)*tilew;
                posy=(j-1)*tileh;
                if i==num_tilew && j==num_tileh
                    tilewout=width-posx;
                    tilehout=height-poxy;
                elseif i==num_tilew
                    tilewout=width-posx;
                    tilehout=tileh;
                elseif j==num_tileh
                    tilewout=tilew;
                    tilehout=height-posy;
                else
                    tilewout=tilew;
                    tilehout=tileh;
                end
                outpng=strcat(outpngbase,'_i',int2str(i),'_j',int2str(j),'.png');
                [s2, ~]=dos(['openslide-write-png.exe',' "',inpath,'" ',int2str(posx),' ',int2str(posy),' ',level,' ',int2str(tilewout),' ',int2str(tilehout),' ','"',outpng,'"']);
                if s2~=0
                    msgbox('.svs file did not write');
                end
                
                %%%Convert .png to .tif
                pngimg=imread(outpng,'PNG');
                savetif=strrep(outpng,'.png','.tif');
                imwrite(pngimg,savetif,'tif');
                delete(outpng)
            end
        end        
    end
end
end


